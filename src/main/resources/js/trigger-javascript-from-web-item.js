AJS.toInit(function()
{
    AJS.$('#my-link-here').on('click', function(evt) // my-link-here is the linkId from atlassian-plugin.xml
    {
        evt.preventDefault();

        AJS.$.get(AJS.contextPath() + '/rest/my-rest-end/1.0/time/fetch', function(data) // /my-rest-end comes from the path attribute in the atlassian-plugin.xml.
        // 1.0 comes from the version attribute in the atlassian-plugin.xml
        // /time is the top @Path in CurrentDateEndPoint
        // /fetch is the annotation of the method
        {

            require(['aui/flag'], function(flag)
            {
                var myFlag = flag({
                   type: 'info',
                   title: 'Current time',
                   persistent: false,
                   body: 'The current time is '+ data.date
                });
            });
        });
    });
});