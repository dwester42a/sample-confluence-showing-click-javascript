package example.rest;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 4/13/17.
 */
@Path("/time")
public class CurrentDateEndPoint
{


    @Path("/fetch")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response fetchCurrentTime()
    {
        Date d = new Date();
        Map<String, String> values = new HashMap<String, String>();
        values.put("date", d.toString());
        return Response.ok( values).build();
    }
}
